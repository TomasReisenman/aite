
const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: 'docs', component: () => import('pages/Index.vue') },
      { path: 'apartments', component: () => import('pages/Apartments.vue') },
      { path: 'petitions', component: () => import('pages/Petitions.vue') },
      { path: 'petitions/:petitionId', props: true , component: () => import('pages/PetitionDetail.vue') },
      { path: '', component: () => import('pages/Stats.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
