import Vue from 'vue'
import Vuex from 'vuex'

import apartments from './store-apartments'
import petitions from './store-petitions'

Vue.use(Vuex)

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      apartments,
      petitions
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: false
  })

  /*
    if we want some HMR magic for it, we handle
    the hot update like below. Notice we guard this
    code with "process.env.DEV" -- so this doesn't
    get into our production build (and it shouldn't).
  */

/*   if (process.env.DEV && module.hot) {
    module.hot.accept(['./apartment-store'], () => {
      const apartments = require('./apartment-store').default
      Store.hotUpdate({ modules: { apartment-store: apartments } })
    })
  } */

  return Store
}