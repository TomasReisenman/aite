const state = {
  apartments: [
    {
      id: 1,
      address: 'John Doe',
      description: 'Amazing',
      rooms: 3,
      rentPrice: 500,
      state: "libre",
      university: "UBA"
    },
    {
      id: 2,
      address: 'Bob Williams',
      description: 'Great',
      rooms: 3,
      rentPrice: 500,
      state: "libre",
      university: "UBA"
    },
    {
      id: 3,
      address: 'Shannon Jackson',
      description: 'OK',
      rooms: 3,
      rentPrice: 500,
      state: "libre",
      university: "UBA"
  
    },
    {
      id: 4,
      address: 'John Doe',
      description: 'Amazing',
      rooms: 3,
      rentPrice: 500,
      state: "libre",
      university: "UBA"
    },
    {
      id: 5,
      address: 'Bob Williams',
      description: 'Great',
      rooms: 3,
      rentPrice: 500,
      state: "libre",
      university: "UBA"
    },
    {
      id: 6,
      address: 'Shannon Jackson',
      description: 'OK',
      rooms: 3,
      rentPrice: 500,
      state: "libre",
      university: "UBA"
  
    },
    {
      id: 7,
      address: 'John Doe',
      description: 'Amazing',
      rooms: 3,
      rentPrice: 500,
      state: "ocupado",
      university: "UBA"
    },
    {
      id: 8,
      address: 'Bob Williams',
      description: 'Great',
      rooms: 3,
      rentPrice: 500,
      state: "libre",
      university: "UBA"
    },
    {
      id: 9,
      address: 'Shannon Jackson',
      description: 'OK',
      rooms: 3,
      rentPrice: 500,
      state: "ocupado",
      university: "UBA"
  
    },
    {
      id: 10,
      address: 'John Doe',
      description: 'Amazing',
      rooms: 3,
      rentPrice: 500,
      state: "libre",
      university: "UBA"
    },
    {
      id: 11,
      address: 'Bob Williams',
      description: 'Great',
      rooms: 3,
      rentPrice: 500,
      state: "libre",
      university: "UBA"
    },
    {
      id: 12,
      address: 'Shannon Jackson',
      description: 'OK',
      rooms: 3,
      rentPrice: 500,
      state: "libre",
      university: "UBA"
  
    },
    {
      id: 13,
      address: 'John Doe',
      description: 'Amazing',
      rooms: 3,
      rentPrice: 500,
      state: "libre",
      university: "UBA"
    },
    {
      id: 14,
      address: 'Bob Williams',
      description: 'Great',
      rooms: 3,
      rentPrice: 500,
      state: "libre",
      university: "UBA"
    },
    {
      id: 15,
      address: 'Shannon Jackson',
      description: 'OK',
      rooms: 3,
      rentPrice: 500,
      state: "libre",
      university: "UBA"
  
    }
  ]
}

const mutations = {
  deleteApartment(state, payload) {
    state.apartments = state.apartments.filter(apartment => apartment.id != payload)
  },
  addApartment(state, payload) {
    state.apartments.push(payload)
  },
  updateApartment(state,payload){
    let indexToUpdate = state.apartments.findIndex(apartment => apartment.id === payload.id);    
    Object.assign(state.apartments[indexToUpdate],payload)
      
  }
}

const actions = {
  deleteApartment({ commit }, payload) {
    commit('deleteApartment', payload)
  },
  addApartment({commit}, payload){
    commit('addApartment', payload)
  },
  updateApartment({commit}, payload){
    commit('updateApartment', payload)
  }
}

const getters = {
  apartments: (state) => {
    return state.apartments
  },
  getAllMatches(state){
    return (petition,page) => state.apartments.filter(apartment => isAMatch(petition,apartment))
    .slice(getFirstIndex(page,2),getSecondIndex(page,2));
  },
  maxPages(state){
    return (petition) => Math.round((state.apartments.filter(apartment => isAMatch(petition,apartment)).length)/2)
  }

}

function isAMatch(petition, apartment){
  return (petition.rooms <= apartment.rooms && 
      petition.budget >= apartment.rentPrice &&
      apartment.state === 'libre' &&
      petition.university === apartment.university)
  
}

function getFirstIndex(page, valuesPerPage){
  return (page -1) * valuesPerPage;
}

function getSecondIndex(page , valuesPerPage){
  return (page * valuesPerPage);
}




export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters

}