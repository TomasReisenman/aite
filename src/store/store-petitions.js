const state = {
  petitions: [
    {
      id: 1,
      contact: '59909',
      rooms: 3,
      budget: 500,
      state: "ocupado",
      university: "UBA"
    },
    {
      id: 2,
      contact: '59909',
      rooms: 2,
      budget: 200,
      state: "buscando",
      university: "UBA"
    },
    {
      id: 3,
      contact: '59909',
      rooms: 3,
      budget: 1300,
      state: "buscando",
      university: "UBA"
    },
    {
      id: 4,
      contact: '59909',
      rooms: 2,
      budget: 700,
      state: "ocupado",
      university: "UBA"
    },
    {
      id: 5,
      contact: '59909',
      rooms: 3,
      budget: 900,
      state: "buscando",
      university: "UCEMA"
    },
    {
      id: 6,
      contact: '59909',
      rooms: 2,
      budget: 700,
      state: "ocupado",
      university: "ORT"
    },
    {
      id: 7,
      contact: '59909',
      rooms: 3,
      budget: 900,
      state: "buscando",
      university: "ORT"
    }
  
  ]
}

const mutations = {
  deletePetition(state, payload) {
    state.petitions = state.petitions.filter(petition => petition.id != payload)
  },
  addPetition(state, payload) {
    state.petitions.push(payload)
  },
  updatePetition(state,payload){
    
    let indexToUpdate = state.petitions.findIndex(petition => petition.id === payload.id);    
    Object.assign(state.petitions[indexToUpdate],payload)
      
  }
}

const actions = {
  deletePetition({ commit }, payload) {
    commit('deletePetition', payload)
  },
  addPetition({commit}, payload){
    commit('addPetition', payload)
  },
  updatePetition({commit}, payload){
    commit('updatePetition', payload)
  }
}

const getters = {
  petitions: (state) => {
    return state.petitions
  },
  stats:(state) => {
    return groupPetionsByUniversity()
  },
   getPetitionById(state){
    return (id) => state.petitions.find(petition => petition.id === id)
  } 

}

function groupPetionsByUniversity(){

  let dataForReport = []; 

  const data = state.petitions;
  
  let universities = data.reduce((groups, item) =>{
      const val = item['university']
      groups[val] = groups[val] || []
      groups[val].push(item)
      return groups
    }, {})

    for (const key of Object.keys(universities)) {

      let currentUniversity = universities[key];

      let universityStats = {
          university : key,
          number:currentUniversity.length,
          avgBudget: (currentUniversity.reduce((previousValue,currentValue) => { 
              return previousValue + currentValue.budget},0))/(currentUniversity.length),
          avgRooms: (currentUniversity.reduce((previousValue,currentValue) => { 
                  return previousValue + currentValue.rooms},0))/(currentUniversity.length),
          petitionsWaiting: currentUniversity
                              .filter( petition => petition.state === 'buscando')
                              .length

      }

      dataForReport.push(universityStats);
  }

  return dataForReport;
}


export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters

}